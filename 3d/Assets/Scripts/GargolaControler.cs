using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GargolaControler : MonoBehaviour
{
    //private float anguloRotacion=1;
    //public int anguloMin;
    public int anguloMax;
    public GameObject player;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        //var angles = transform.rotation.eulerAngles;
        //angles.y += Time.deltaTime * 10;
        //transform.rotation = Quaternion.Euler(0,anguloMin,0);
        //StartCoroutine(ObjectRotate());
        float angle = Mathf.Sin(Time.time) * anguloMax;
        transform.rotation = Quaternion.AngleAxis(angle, Vector3.up);
    }
    //IEnumerator ObjectRotate()
    //{
        
    //    while (true)
    //    {
    //        float angle = Mathf.Sin(Time.time) * anguloMax;
    //        transform.rotation = Quaternion.AngleAxis(angle, Vector3.up);

    //        yield return null;
    //    }
    //}
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            Debug.Log("h");
            Destroy(player);
        }
    }
}
