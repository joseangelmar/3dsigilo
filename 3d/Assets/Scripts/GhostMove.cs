using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class GhostMove : MonoBehaviour
{
    public NavMeshAgent navMeshAgent;
    public Transform destinationObgect;
    public List<Transform> destinationObgects;
    private int currentDestinationIndex;
    // Start is called before the first frame update
    void Start()
    {
        navMeshAgent.SetDestination(destinationObgects[0].position);
        currentDestinationIndex = 0;
    }

    // Update is called once per frame
    void Update()
    {
        //if (transform.position == destinationObgects[currentDestinationIndex].position)
        if(navMeshAgent.remainingDistance<=0.1f)
        {
            currentDestinationIndex += 1;
            if (currentDestinationIndex == destinationObgects.Count)
            {
                currentDestinationIndex = 0;
            }
            navMeshAgent.SetDestination(destinationObgects[currentDestinationIndex].position);           
        }
    }
}
